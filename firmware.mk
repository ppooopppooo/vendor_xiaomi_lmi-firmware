# Firmware
PRODUCT_COPY_FILES += \
	vendor/xiaomi/lmi-firmware/proprietary/abl.elf:install/firmware-update/abl.elf \
	vendor/xiaomi/lmi-firmware/proprietary/NON-HLOS.bin:install/firmware-update/NON-HLOS.bin \
	vendor/xiaomi/lmi-firmware/proprietary/cmnlib.mbn:install/firmware-update/cmnlib.mbn \
	vendor/xiaomi/lmi-firmware/proprietary/storsec.mbn:install/firmware-update/storsec.mbn \
	vendor/xiaomi/lmi-firmware/proprietary/km4.mbn:install/firmware-update/km4.mbn \
	vendor/xiaomi/lmi-firmware/proprietary/featenabler.mbn:install/firmware-update/featenabler.mbn \
	vendor/xiaomi/lmi-firmware/proprietary/xbl_5.elf:install/firmware-update/xbl_5.elf \
	vendor/xiaomi/lmi-firmware/proprietary/cmnlib64.mbn:install/firmware-update/cmnlib64.mbn \
	vendor/xiaomi/lmi-firmware/proprietary/xbl_config_4.elf:install/firmware-update/xbl_config_4.elf \
	vendor/xiaomi/lmi-firmware/proprietary/qupv3fw.elf:install/firmware-update/qupv3fw.elf \
	vendor/xiaomi/lmi-firmware/proprietary/tz.mbn:install/firmware-update/tz.mbn \
	vendor/xiaomi/lmi-firmware/proprietary/aop.mbn:install/firmware-update/aop.mbn \
	vendor/xiaomi/lmi-firmware/proprietary/xbl_4.elf:install/firmware-update/xbl_4.elf \
	vendor/xiaomi/lmi-firmware/proprietary/xbl_config_5.elf:install/firmware-update/xbl_config_5.elf \
	vendor/xiaomi/lmi-firmware/proprietary/uefi_sec.mbn:install/firmware-update/uefi_sec.mbn \
	vendor/xiaomi/lmi-firmware/proprietary/hyp.mbn:install/firmware-update/hyp.mbn \
	vendor/xiaomi/lmi-firmware/proprietary/dspso.bin:install/firmware-update/dspso.bin \
	vendor/xiaomi/lmi-firmware/proprietary/devcfg.mbn:install/firmware-update/devcfg.mbn \
	vendor/xiaomi/lmi-firmware/proprietary/BTFM.bin:install/firmware-update/devcfg.mbn
